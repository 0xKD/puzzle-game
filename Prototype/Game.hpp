#pragma once
#include "Prototype.hpp"
#include "Tilemap.hpp"
#include "Player.hpp"
#include "LevelEditor.hpp"

class Game
{
public:
    static SDL_Texture* rock_sprite;
    static SDL_Texture* bomb_sprite;
    static SDL_Texture* up_sprite;
    static SDL_Texture* down_sprite;
    static SDL_Texture* right_sprite;
    static SDL_Texture* left_sprite;
    static SDL_Texture* knob_on_sprite;
    static SDL_Texture* knob_off_sprite;
    static SDL_Texture* switch_one_off;
    static SDL_Texture* switch_one_on;
    static SDL_Texture* switch_two_on;
    static SDL_Texture* switch_two_off;
    static SDL_Texture* end_sprite;
    static SDL_Texture* ui_one;
    static SDL_Texture* ui_two;
    static SDL_Texture* dynamite_sprite;
    static TTF_Font* numeric_font;
    
    Tilemap tilemap;
    Editor editor;
    Player player;
    TextSprite num_moves_text;
    TextSprite num_bombs_text;
    TextSprite max_moves_text;
    
    Game(SDL_Renderer* Renderer);
    void PreloadAssets(SDL_Renderer* Renderer);
    void HandleEvents(SDL_Event* e);
    void UpdateAndRender(SDL_Renderer* Renderer);
};
