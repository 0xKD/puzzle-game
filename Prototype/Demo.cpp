#include "Prototype.hpp"
#include "Game.hpp"

static SDL_Window* Window = NULL;
static SDL_Renderer* Renderer = NULL;
static int WindowWidth = TILE_WIDTH*SCREEN_WIDTH;
static int WindowHeight = TILE_WIDTH*SCREEN_HEIGHT;

int main(int argc, char** argv)
{
    if(InitSDL(&Window, &Renderer, "Prototype", WindowWidth, WindowHeight) == 1)
    {
        Game game(Renderer);
       
        SDL_Event e;
        bool running = true;
        
        while(running)
        {
            
            Uint32 start_time = SDL_GetTicks();
            while(SDL_PollEvent(&e) != 0)
            {
                if(e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE)
                {
                    running = false;
                }
                else
                    game.HandleEvents(&e);
                
            }
            SDL_SetRenderDrawColor(Renderer, 0x00, 0x00, 0x00, 0xFF);
            SDL_RenderClear(Renderer);

            game.UpdateAndRender(Renderer);
            
            SDL_RenderPresent(Renderer);
            Uint32 end_time = SDL_GetTicks();
            Uint32 delay_time = end_time - start_time;
            if(delay_time < 17)
            {
                SDL_Delay(17 - delay_time);
            }
        }
    }
    return 0;
}
