#pragma once
#include "Prototype.hpp"
#include "Utils.hpp"
#include "Tilemap.hpp"
#include "Player.hpp"
#include "GameObjects.hpp"

class Editor
{
public:
    static const int ROWS = 5; 
    static const int COLS = 8;
    Tile map[ROWS][COLS];
    MatrixIndex curr_indices;
    Tile* curr_tile;
    CoupledSwitch coupled_switch;
    PortalPair static_portal_pair;
    bool dynamic_portals_enabled;
    bool enabled;
    uint8 move_limit;

    Editor();
    void Toggle(Tilemap* real_map, Player* player);

    // currently being used to just call toggle immediately after load.
    // better ways to do the same? Any reason why this is a Bad Thing?
    void HandleEvents(SDL_Event* e, Tilemap* real_map, Player* player); 
    void Load(Tilemap* real_map, Player* player);
    
    void Save();
    void UpdateAndRender(SDL_Renderer* Renderer);
    MatrixIndex GetIndices(int screen_x, int screen_y);
};
