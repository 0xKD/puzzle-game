#pragma once
#include <SDL.h>
#include <stdint.h>

#define Log SDL_Log
#define GetError SDL_GetError

#define uint32 uint32_t
#define uint16 uint16_t
#define uint8 uint8_t

const uint32 TILE_WIDTH = 150;
const uint32 SCREEN_WIDTH = 9;
const uint32 SCREEN_HEIGHT = 5;
