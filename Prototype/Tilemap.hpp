#pragma once
#include "Prototype.hpp"
#include "Utils.hpp"
#include "GameObjects.hpp"

class PortalPair;
class CoupledSwitch;

class Tile
{
public:
    // 0       0              0                 0
    //                                          
    // 0                0            0                  0
    // PORTAL           KNOB_ON      SWITCH_ON             TWO_OBSTACLES
    // 0       0    0     0      
    // VISIBLE END  BOMB2 BOMB1
    // 0   0  0   0   0 0  0 0
    // DO  UO LO  RO  D U  L R
    uint32 bitfield;
    
    Tile();
};
    
class Tilemap
{
public:
    static const uint32 LEFT = 2;
    static const uint32 RIGHT = 1;
    static const uint32 UP = 4;
    static const uint32 DOWN = 8;
    
    static const uint32 RIGHT_OBSTACLE = 16;
    static const uint32 LEFT_OBSTACLE = 32;
    static const uint32 UP_OBSTACLE = 64;
    static const uint32 DOWN_OBSTACLE = 128;

    static const uint32 BOMB1 = 256;
    static const uint32 BOMB2 = 512;

    static const uint32 END = 1024;
    static const uint32 VISIBLE = 2048;
    static const uint32 TWO_OBSTACLES = 4096; // for level 1

    static const uint32 SWITCH_ON = 8192;
    static const uint32 KNOB_ON = 16384;

    static const uint32 PORTAL = 32768;
    // static const uint32 PORTAL = 65536;
    // static const uint32 KNOB_ON = 131072;
    
    static const int COLUMNS = 8;
    static const int ROWS = 5;

    Tile map[ROWS][COLUMNS];
    Tile init_data[ROWS][COLUMNS];
    CoupledSwitch* coupled_switch;
    PortalPair* portal_pair;
    PortalPair* static_portal_pair;
    
    Tilemap();
    void Reset();

    void AddPortal(uint8 row, uint8 col);
    void DeletePortal(uint8 row, uint8 col);
    void GetPortDest(MatrixIndex* curr_player_position);
    
    void TurnKnob(MatrixIndex* player_position);

    // returns 1 if obstacle blown, 0 if it isnt.
    // two obstacles not taken into account.
    int BlowObstacleAt(MatrixIndex* obs_tile);

    // returns 1 if bomb picked, 0 if not.
    // two bombs not taken into account.
    int PickBombAt(MatrixIndex* bomb_tile);
    
    void UpdateAndRender(SDL_Renderer* Renderer);
};
