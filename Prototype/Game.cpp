#include "Game.hpp"

SDL_Texture* Game::rock_sprite = NULL;
SDL_Texture* Game::bomb_sprite = NULL;
SDL_Texture* Game::up_sprite = NULL;
SDL_Texture* Game::down_sprite = NULL;
SDL_Texture* Game::right_sprite = NULL;
SDL_Texture* Game::left_sprite = NULL;
SDL_Texture* Game::knob_on_sprite = NULL;
SDL_Texture* Game::knob_off_sprite = NULL;
SDL_Texture* Game::switch_one_off = NULL;
SDL_Texture* Game::switch_one_on = NULL;
SDL_Texture* Game::switch_two_on = NULL;
SDL_Texture* Game::switch_two_off = NULL;
SDL_Texture* Game::end_sprite = NULL;
SDL_Texture* Game::ui_one = NULL;
SDL_Texture* Game::ui_two = NULL;
SDL_Texture* Game::dynamite_sprite = NULL;
TTF_Font*    Game::numeric_font = NULL;

Game::Game(SDL_Renderer* Renderer)
{
    PreloadAssets(Renderer);
    player.SetTilemap(&tilemap);
    num_moves_text.Init(numeric_font, Renderer);
    num_bombs_text.Init(numeric_font, Renderer);
    max_moves_text.Init(numeric_font, Renderer);
}

void
Game::HandleEvents(SDL_Event* e)
{
    if(e->type == SDL_KEYDOWN)
    {
        if(e->key.keysym.sym == SDLK_t)
        {
            //editor.Toggle(&tilemap, &player);
        }
        else if(editor.enabled)
        {
            editor.HandleEvents(e, &tilemap, &player);
        }
        else
        {
            player.HandleEvents(e);
        }
    }
}

void
Game::UpdateAndRender(SDL_Renderer* Renderer)
{
    if(editor.enabled)
    {
        editor.UpdateAndRender(Renderer);
    }
    else
    {
        tilemap.UpdateAndRender(Renderer);
        player.UpdateAndRender(Renderer);

        num_moves_text.UpdateAndRender(8*TILE_WIDTH + 50,
                                       TILE_WIDTH,
                                       player.num_moves,
                                       numeric_font, Renderer);

        static SDL_Rect bomb_rect = {8*TILE_WIDTH + 100, 2*TILE_WIDTH, 32, 32};
        SDL_RenderCopy(Renderer, bomb_sprite, NULL, &bomb_rect);
        num_bombs_text.UpdateAndRender(8*TILE_WIDTH + 50,
                                       2*TILE_WIDTH,
                                       player.num_bombs,
                                       numeric_font, Renderer);

        max_moves_text.UpdateAndRender(8*TILE_WIDTH + 50,
                                       4*TILE_WIDTH + 50,
                                       player.move_limit,
                                       numeric_font, Renderer);
    }
}

void
Game::PreloadAssets(SDL_Renderer* Renderer)
{
    rock_sprite = LoadImage("rock.png", Renderer);
    bomb_sprite = LoadImage("bomb.png", Renderer);
    up_sprite = LoadImage("assets/up.png", Renderer);
    down_sprite = LoadImage("assets/down.png", Renderer);
    right_sprite = LoadImage("assets/right.png", Renderer);
    left_sprite = LoadImage("assets/left.png", Renderer);
    knob_off_sprite = LoadImage("assets/switchLeft.png", Renderer);
    knob_on_sprite = LoadImage("assets/switchRight.png", Renderer);
    switch_one_off = LoadImage("assets/buttonBlue.png", Renderer);
    switch_one_on = LoadImage("assets/buttonBlue_pressed.png", Renderer);
    switch_two_off = LoadImage("assets/buttonRed.png", Renderer);
    switch_two_on = LoadImage("assets/buttonRed_pressed.png", Renderer);
    end_sprite = LoadImage("assets/hud_heartFull.png", Renderer);
    ui_one = LoadImage("assets/hud_1.png", Renderer);
    ui_two = LoadImage("assets/hud_2.png", Renderer);
    dynamite_sprite = LoadImageWithColorKey("Dynamite.png", Renderer, 0xFF, 0xFF, 0xFF);
    numeric_font = TTF_OpenFont("shoes_center.ttf", 50);
}
