#pragma once
#include "Prototype.hpp"
#include "Tilemap.hpp"
#include "Utils.hpp"

class Player
{
public:
    Player();
    Player(Tilemap* tm);

    MatrixIndex position;
    SDL_Rect rect;
    Tilemap* tmap;
    bool right;
    bool left;
    bool up;
    bool down;            
    int num_bombs;
    Tile* curr_tile;
    uint8 num_moves;
    uint8 move_limit;
    
    void Init(uint8 max_moves);
    
    void ActionKeyPressed();
    
    void SetTilemap(Tilemap* tm);
    void HandleEvents(SDL_Event* e);
    void UpdateAndRender(SDL_Renderer* Renderer);
};
