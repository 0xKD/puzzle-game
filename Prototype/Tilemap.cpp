#include "Tilemap.hpp"
#include "Utils.hpp"
#include "Game.hpp"

Tile::Tile()
{
    bitfield = 0;
}

Tilemap::Tilemap()
{
    for(int i = 0; i < ROWS; i++)
    {
        for(int j = 0; j < COLUMNS; j++)
        {
            map[i][j].bitfield = 0;
        }
    }
    
    portal_pair = (PortalPair*)malloc(sizeof(PortalPair));
    portal_pair->portal_1 = {255, 255};
    portal_pair->portal_2 = {255, 255};
    portal_pair->enabled = false;
    
    static_portal_pair = (PortalPair*)malloc(sizeof(PortalPair));
    static_portal_pair->portal_1 = {255, 255};
    static_portal_pair->portal_2 = {255, 255};
    static_portal_pair->enabled = false;
    
    coupled_switch = (CoupledSwitch*)malloc(sizeof(CoupledSwitch));
    coupled_switch->knob1_indices = {255, 255};
    coupled_switch->knob2_indices = {255, 255};
    coupled_switch->switch_indices = {255, 255};
    coupled_switch->affected_indices = {255, 255};
    coupled_switch->obstacle_affected = 0;
    coupled_switch->enabled = false;
    coupled_switch->knob1_on = false;
    coupled_switch->knob2_on = false;
}

void
Tilemap::Reset()
{
    for(int row = 0; row < ROWS; row++)
    {
        for(int col = 0; col < COLUMNS; col++)
        {
            map[row][col] = init_data[row][col];
        }
    }
    if(coupled_switch->enabled)
        coupled_switch->Setup();
    if(portal_pair->enabled)
        portal_pair->Setup(this);
}

void
Tilemap::AddPortal(uint8 row, uint8 col)
{
    uint32 portal_mask = UP_OBSTACLE | DOWN_OBSTACLE |
        RIGHT_OBSTACLE | LEFT_OBSTACLE | BOMB1;

    bool portal_on_knob = false; 
    if(coupled_switch->enabled)
    {
        MatrixIndex temp = {row, col};
        portal_on_knob = (EQUAL_INDEX(&coupled_switch->knob1_indices, &temp) ||
                          EQUAL_INDEX(&coupled_switch->knob2_indices, &temp) ||
                          EQUAL_INDEX(&coupled_switch->switch_indices, &temp));
    }
    
    if(!portal_on_knob && IS_OFF(map[row][col].bitfield, portal_mask))
    {
        // if portal_1 not set, set it
        if(!(VALID_INDEX(&portal_pair->portal_1)))
        {
            portal_pair->portal_1.row = row;
            portal_pair->portal_1.col = col;
            map[row][col].bitfield ^= PORTAL;
        }
        // else set portal 2
        else
        {
            portal_pair->portal_2.row = row;
            portal_pair->portal_2.col = col;
            map[row][col].bitfield ^= PORTAL;
        }
    }
}

void
Tilemap::DeletePortal(uint8 row, uint8 col)
{
    MatrixIndex temp = {row, col};
    if(EQUAL_INDEX(&portal_pair->portal_1, &temp))
    {
        map[row][col].bitfield ^= PORTAL;
        portal_pair->portal_1 = {255, 255};
    }
    else if(EQUAL_INDEX(&portal_pair->portal_2, &temp))
    {
        map[row][col].bitfield ^= PORTAL;
        portal_pair->portal_2 = {255, 255};
    }
}

void
Tilemap::GetPortDest(MatrixIndex* curr_player_position)
{
    if(portal_pair->enabled)
    {
        // the valid index check needs to be done because these portals are dynamic.
        // the enabled flag is set by the editor only to signify that they can be used.
        if(VALID_INDEX(&portal_pair->portal_2) &&
           EQUAL_INDEX(curr_player_position, &portal_pair->portal_1))
        {
            curr_player_position->row = portal_pair->portal_2.row;
            curr_player_position->col = portal_pair->portal_2.col;
        }
        else if(VALID_INDEX(&portal_pair->portal_1) &&
                EQUAL_INDEX(curr_player_position, &portal_pair->portal_2))
        {
            curr_player_position->row = portal_pair->portal_1.row;
            curr_player_position->col = portal_pair->portal_1.col;
        }    
    }
    else
    {
        if(EQUAL_INDEX(curr_player_position, &static_portal_pair->portal_1))
        {
            curr_player_position->row = static_portal_pair->portal_2.row;
            curr_player_position->col = static_portal_pair->portal_2.col;
        }
        else if(EQUAL_INDEX(curr_player_position, &static_portal_pair->portal_2))
        {
            curr_player_position->row = static_portal_pair->portal_1.row;
            curr_player_position->col = static_portal_pair->portal_1.col;
        }
    }
}

void
Tilemap::TurnKnob(MatrixIndex* player_position)
{
    if(EQUAL_INDEX(player_position, &(coupled_switch->knob1_indices)))
    {
        if(coupled_switch->knob1_on)
            coupled_switch->knob1_on = false;
        else
            coupled_switch->knob1_on = true;
    }
    else if(EQUAL_INDEX(player_position, &(coupled_switch->knob2_indices)))
    {
        if(coupled_switch->knob2_on)
            coupled_switch->knob2_on = false;
        else
            coupled_switch->knob2_on = true;
    }
}

int
Tilemap::PickBombAt(MatrixIndex* bomb_tile)
{
   
    map[bomb_tile->row][bomb_tile->col].bitfield ^= Tilemap::BOMB1;
    return 1;
}

int
Tilemap::BlowObstacleAt(MatrixIndex* obs_tile)
{
    if(EQUAL_INDEX(obs_tile, &coupled_switch->affected_indices))
    {
        return 0;
    }
    else
    {
        uint32 bf = map[obs_tile->row][obs_tile->col].bitfield;
        uint32 mask = UP_OBSTACLE | DOWN_OBSTACLE |
            LEFT_OBSTACLE | RIGHT_OBSTACLE;
        switch(bf & mask)
        {
        case UP_OBSTACLE:
        {
            bf ^= UP_OBSTACLE;
        }
        break;

        case DOWN_OBSTACLE:
        {
            bf ^= DOWN_OBSTACLE;
        }
        break;

        case RIGHT_OBSTACLE:
        {
            bf ^= RIGHT_OBSTACLE;
        }
        break;

        case LEFT_OBSTACLE:
        {
            bf ^= LEFT_OBSTACLE;
        }
        break;
        } // end obstacle mask switch
        
        map[obs_tile->row][obs_tile->col].bitfield = bf;
        return 1;
    }
}

void
Tilemap::UpdateAndRender(SDL_Renderer* Renderer)
{
    SDL_SetRenderDrawColor(Renderer, 0x00, 0xFF, 0x00, 0xFF);
    for(int i = 1; i < COLUMNS + 1; i++)
    {
        SDL_RenderDrawLine(Renderer, TILE_WIDTH*i, 0, TILE_WIDTH*i, TILE_WIDTH*ROWS);
    }
    for(int i = 1; i < ROWS; i++)
    {
        SDL_RenderDrawLine(Renderer, 0, TILE_WIDTH*i, TILE_WIDTH*COLUMNS, TILE_WIDTH*i);
    }

    if(coupled_switch->enabled)
    {
        bool knob1_visible = IS_ON(map[coupled_switch->knob1_indices.row][coupled_switch->knob1_indices.col].bitfield,
                                   Tilemap::VISIBLE);
        bool knob2_visible = IS_ON(map[coupled_switch->knob2_indices.row][coupled_switch->knob2_indices.col].bitfield,
                                   Tilemap::VISIBLE);
        bool switch_visible = IS_ON(map[coupled_switch->switch_indices.row][coupled_switch->switch_indices.col].bitfield,
                                    Tilemap::VISIBLE);
        
        coupled_switch->UpdateAndRender(Renderer, knob1_visible, knob2_visible, switch_visible);
        // NOTE: this is here because i am modifying the tilemap, many such general operations
        // should be shifted to tilemap. so that the tilemap contains a lot of the game logic.
        
        uint32* affected_bf = &map[coupled_switch->affected_indices.row][coupled_switch->affected_indices.col].bitfield;
        // NOTE: this check ensures that affected obstacles are not blown by normal bombs. 
        if(coupled_switch->knob1_on && coupled_switch->knob2_on)
        {
            // mask all obstacle bits off
            // assumes only affected obstacle to be in
            // affected tile.
            *affected_bf &= 0xFFFFFF0F;
        }
        else
        {
            if(IS_OFF(*affected_bf, coupled_switch->obstacle_affected))
            {
                *affected_bf ^= coupled_switch->obstacle_affected;
            }
        }
    }
    
    if(portal_pair->enabled)
    {
        portal_pair->UpdateAndRender(Renderer, true, true);
    }

    if(static_portal_pair->enabled)
    {
        bool sp1_visible = IS_ON(map[static_portal_pair->portal_1.row][static_portal_pair->portal_1.col].bitfield,
                                 Tilemap::VISIBLE);
        bool sp2_visible = IS_ON(map[static_portal_pair->portal_2.row][static_portal_pair->portal_2.col].bitfield,
                                 Tilemap::VISIBLE);
        static_portal_pair->UpdateAndRender(Renderer, sp1_visible, sp2_visible);
    }
    
    for(int row = 0; row < ROWS; row++)
    {
        for(int col = 0; col < COLUMNS; col++)
        {
            uint32 bf = map[row][col].bitfield;
            SDL_Rect temp = {0};

            if(IS_ON(bf, VISIBLE))
            {
                // displaying end
                if(IS_ON(bf, END))
                {
                    temp = {TILE_WIDTH*col + TILE_WIDTH/2 + 16,
                            TILE_WIDTH*row + TILE_WIDTH/2 - 16,
                            32, 32};
                    SDL_RenderCopy(Renderer, Game::end_sprite, NULL, &temp);
                }

                
                // displaying arrows
                if(IS_ON(bf, RIGHT))
                {
                    temp = {TILE_WIDTH*(col+1) - 48,
                            TILE_WIDTH*row + TILE_WIDTH/2 - 16,
                            32, 32};
                    SDL_RenderCopy(Renderer, Game::right_sprite, NULL, &temp);
                }
                if(IS_ON(bf, LEFT))
                {
                    temp = {TILE_WIDTH*col + 16,
                            TILE_WIDTH*row + TILE_WIDTH/2 - 16,
                            32, 32};
                    SDL_RenderCopy(Renderer, Game::left_sprite, NULL, &temp);
                }
                if(IS_ON(bf, DOWN))
                {
                    temp = { TILE_WIDTH*col + TILE_WIDTH/2 - 16,
                             TILE_WIDTH*(row+1) - 48,
                             32, 32 };
                    SDL_RenderCopy(Renderer, Game::down_sprite, NULL, &temp);
                }
                if(IS_ON(bf, UP))
                {
                    temp = { TILE_WIDTH*col + TILE_WIDTH/2 - 16,
                             TILE_WIDTH*row + 16,
                             32, 32 };
                    SDL_RenderCopy(Renderer, Game::up_sprite, NULL, &temp);
                }
                
                //   displaying bombs
                uint16 mask = BOMB1 | BOMB2;
                if(IS_ON(bf, mask))
                {
                    // display two bombs
                    temp = {TILE_WIDTH*col + 16,
                            TILE_WIDTH*row + 16,
                            32, 32};
                    SDL_RenderCopy(Renderer, Game::bomb_sprite, NULL, &temp);
                    temp.x = TILE_WIDTH*(col+1) - 48;
                    SDL_RenderCopy(Renderer, Game::bomb_sprite, NULL, &temp);
                }
                else if((bf & mask) == BOMB1 ||
                        (bf & mask) == BOMB2)
                {
                    temp = {TILE_WIDTH*col + 16,
                            TILE_WIDTH*row + 16,
                            32, 32};
                    SDL_RenderCopy(Renderer, Game::bomb_sprite, NULL, &temp);
                }

                {
                    uint32 mask = UP_OBSTACLE | RIGHT_OBSTACLE | LEFT_OBSTACLE | DOWN_OBSTACLE;

                    MatrixIndex obs_tile_index = {row, col};
                    SDL_Rect crop_rect = {10, 14, 38, 60};
                    switch((bf & mask))
                    {
                    case UP_OBSTACLE:
                    {
                        temp = {TILE_WIDTH*col + TILE_WIDTH/2 - 16,
                                TILE_WIDTH*row,
                                32, 32};
                        if(VALID_INDEX(&coupled_switch->affected_indices) &&
                           EQUAL_INDEX(&coupled_switch->affected_indices, &obs_tile_index))
                        {
                            temp.y += 20;
                            temp.w = 38;
                            temp.h = 60;
                            SDL_RenderCopy(Renderer, Game::dynamite_sprite, &crop_rect, &temp);
                        }
                        else
                        {
                            SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);

                            if(IS_ON(bf, TWO_OBSTACLES))
                            {
                                temp.y += 32;
                                bf ^= TWO_OBSTACLES;
                                SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                            }
                        }
                
                    }
                    break;

                    case DOWN_OBSTACLE:
                    {
                        temp = {TILE_WIDTH*col + TILE_WIDTH/2 - 16,
                                TILE_WIDTH*(row+1) - 32,
                                32, 32};
                        if(VALID_INDEX(&coupled_switch->affected_indices) &&
                           EQUAL_INDEX(&coupled_switch->affected_indices, &obs_tile_index))
                        {
                            temp.y -= 20;
                            temp.w = 38;
                            temp.h = 60;
                            SDL_RenderCopy(Renderer, Game::dynamite_sprite, &crop_rect, &temp);
                        }
                        else
                        {
                            SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                            if(IS_ON(bf, TWO_OBSTACLES))
                            {
                                temp.y -= 32;
                                bf ^= TWO_OBSTACLES;
                                SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                            }
                        }
                    }
                    break;

                    case RIGHT_OBSTACLE:
                    {
                        temp = {TILE_WIDTH*(col+1) - 32,
                                TILE_WIDTH*row + TILE_WIDTH/2 - 16,
                                32, 32};
                        if(VALID_INDEX(&coupled_switch->affected_indices) &&
                           EQUAL_INDEX(&coupled_switch->affected_indices, &obs_tile_index))
                        {
                            temp.x -= 20;
                            temp.w = 38;
                            temp.h = 60;
                            SDL_RenderCopy(Renderer, Game::dynamite_sprite, &crop_rect, &temp);
                        }
                        else
                        {
                            SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                            if(IS_ON(bf, TWO_OBSTACLES))
                            {
                                temp.x -= 32;
                                bf ^= TWO_OBSTACLES;
                                SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                            }
                        }
                    }
                    break;

                    case LEFT_OBSTACLE:
                    {
                        temp = {TILE_WIDTH*col,
                                TILE_WIDTH*row + TILE_WIDTH/2 - 16,
                                32, 32};
                        if(VALID_INDEX(&coupled_switch->affected_indices) &&
                           EQUAL_INDEX(&coupled_switch->affected_indices, &obs_tile_index))
                        {
                            temp.x += 20;
                            temp.w = 38;
                            temp.h = 60;
                            SDL_RenderCopy(Renderer, Game::dynamite_sprite, &crop_rect, &temp);
                        }
                        else
                        {
                            SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                            if(IS_ON(bf, TWO_OBSTACLES))
                            {
                                temp.x += 32;
                                bf ^= TWO_OBSTACLES;
                                SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                            }
                        }
                    }
                    break;
                    } // switch for obstacle display
                } // local anonymous block for obstacles
            } // visibilty test
        } // inner for loop - columns 
    } // outer for loop - rows
} // function body
