#include "LevelEditor.hpp"
#include "Game.hpp"
#include <stdio.h>  // sprintf, FILE, fprintf, fscanf
#include <stdlib.h> // atoi

/*
  NOTE:
  Currently the editor does not care if a bomb and
  an obstacle is placed on the same tile, but player
  handle events logic does not allow it.
*/

Editor::Editor()
{
    curr_indices = {0, 0};
    enabled = true;
    dynamic_portals_enabled = false;
    curr_tile = &map[curr_indices.row][curr_indices.col];
}

void
Editor::Toggle(Tilemap* tilemap, Player* player)
{
    if(enabled)
    {
        for(int row = 0; row < ROWS; row++)
        {
            for(int col = 0; col < COLS; col++)
            {
                tilemap->init_data[row][col] = map[row][col];
                tilemap->map[row][col] = map[row][col];
            }
        }
        
        if(VALID_INDEX(&coupled_switch.knob1_indices) &&
           VALID_INDEX(&coupled_switch.knob2_indices) &&
           VALID_INDEX(&coupled_switch.switch_indices) &&
           VALID_INDEX(&coupled_switch.affected_indices))
        {
            coupled_switch.enabled = true;
            coupled_switch.knob1_on = false;
            coupled_switch.knob2_on = false;
            *(tilemap->coupled_switch) = coupled_switch;
        }
        else
        {
            coupled_switch.enabled = false;
        }

        // set to true or false depending on pressing caps lock
        tilemap->portal_pair->enabled = dynamic_portals_enabled;
        
        if(VALID_INDEX(&static_portal_pair.portal_1) &&
           VALID_INDEX(&static_portal_pair.portal_2))
        {
            static_portal_pair.enabled = true;
            // NOTE: assumes a level cant have dynamic
            // and static portals both.
            dynamic_portals_enabled = false;
            tilemap->portal_pair->enabled = dynamic_portals_enabled;
            *(tilemap->static_portal_pair) = static_portal_pair;
            
            tilemap->init_data[static_portal_pair.portal_1.row][static_portal_pair.portal_1.col].bitfield |= Tilemap::PORTAL;
            tilemap->init_data[static_portal_pair.portal_2.row][static_portal_pair.portal_2.col].bitfield |= Tilemap::PORTAL;

            tilemap->map[static_portal_pair.portal_1.row][static_portal_pair.portal_1.col].bitfield |= Tilemap::PORTAL;
            tilemap->map[static_portal_pair.portal_2.row][static_portal_pair.portal_2.col].bitfield |= Tilemap::PORTAL;
        }
        else if(dynamic_portals_enabled)
        {
            tilemap->portal_pair->Setup(tilemap);
        }
        
        player->Init(move_limit);
        enabled = false;
    }
    else
    {
        enabled = true;
    }
}

void
Editor::Save()
{
    Log("Enter level number.");
    int level_number = 0;
    scanf("%d", &level_number);
    char fname[20];
    sprintf(fname, "Levels/Level%d.txt", level_number);
    FILE* fptr;
    fptr = fopen(fname, "w");
    char map_data[100];

    // max moves
    sprintf(map_data, "MAX_MOVES=%d\n", move_limit);
    fprintf(fptr, "%s", map_data);

    // map data
    for(int row = 0; row < ROWS; row++)
    {
        for(int col = 0; col < COLS; col++)
        {
            // NOTE: whitespaces break how fscanf scans data hence no whitespaces.
            sprintf(map_data, "map[%d][%d].bitfield=%d\n", row, col, map[row][col].bitfield);
            fprintf(fptr, "%s", map_data);
        }
    }

    // saving switch data
    int enabled_flag = 0;
    if(coupled_switch.enabled) // this is set only when you toggle
    {
        enabled_flag = 1;
        fprintf(fptr, "COUPLED_SWITCH_ENABLED=%d\n", enabled_flag);
            
        sprintf(map_data, "knob1_position=%d,%d\n", coupled_switch.knob1_indices.row, coupled_switch.knob1_indices.col);
        fprintf(fptr, "%s", map_data);

        sprintf(map_data, "knob2_position=%d,%d\n", coupled_switch.knob2_indices.row, coupled_switch.knob2_indices.col);
        fprintf(fptr, "%s", map_data);

        sprintf(map_data, "switch_position=%d,%d\n", coupled_switch.switch_indices.row, coupled_switch.switch_indices.col);
        fprintf(fptr, "%s", map_data);

        sprintf(map_data, "affected_position=%d,%d\n", coupled_switch.affected_indices.row, coupled_switch.affected_indices.col);
        fprintf(fptr, "%s", map_data);

        sprintf(map_data, "obstacle_affected=%d\n", coupled_switch.obstacle_affected);
        fprintf(fptr, "%s", map_data);
    }
    else
        fprintf(fptr, "COUPLED_SWITCH_ENABLED=%d\n", enabled_flag);

    // saving static portal data
    if(static_portal_pair.enabled)
    {
        enabled_flag = 1;
        fprintf(fptr, "STATIC_PORTALS_ENABLED=%d\n", enabled_flag);

        sprintf(map_data, "sp1_position=%d,%d\n", static_portal_pair.portal_1.row, static_portal_pair.portal_1.col);
        fprintf(fptr, "%s", map_data);

        sprintf(map_data, "sp2_position=%d,%d\n", static_portal_pair.portal_2.row, static_portal_pair.portal_2.col);
        fprintf(fptr,"%s", map_data);
    }
    else
    {
        enabled_flag = 0;
        fprintf(fptr, "STATIC_PORTALS_ENABLED=%d\n", enabled_flag);

        if(dynamic_portals_enabled)
        {
            enabled_flag = 1;
            fprintf(fptr, "DYNAMIC_PORTALS_ENABLED=%d\n", enabled_flag);
        }
        else
        {
            enabled_flag = 0;
            fprintf(fptr, "DYNAMIC_PORTALS_ENABLED=%d\n", enabled_flag);
        }
    }
        
    fclose(fptr);
}

void
Editor::Load(Tilemap* real_map, Player* player)
{
    Log("Enter level file to load.");
    int level_number = 0;
    scanf("%d", &level_number);
    char fname[20];
    FILE* fptr;
    sprintf(fname, "Levels/Level%d.txt", level_number);
    fptr = fopen(fname, "r");

    uint8 row = 0;
    uint8 col = 0;
    int bitfield = 0;
    char map_data[100];
    int num_elems = ROWS*COLS;

    // reading max moves
    fscanf(fptr, "%s", map_data);
    move_limit = atoi(&map_data[10]);
    
    // reading map data
    while(num_elems != 0)
    {
        /*
          map[%d][%d].bitfield=%d
              4   7            19
         Same logic for switches.
         */
        
        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[4]);
        col = atoi(&map_data[7]);
        bitfield = atoi(&map_data[19]);
        map[row][col].bitfield = bitfield;
        num_elems -= 1;
    }
    
    // read switch data
    // COUPLED_SWITCH_ENABLED=%d
    //                        23
    int switch_enabled = 0;
    fscanf(fptr, "%s", map_data);
    switch_enabled = atoi(&map_data[23]);
    if(switch_enabled)
    {
        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[15]);
        col = atoi(&map_data[17]);
        coupled_switch.knob1_indices = {row, col};

        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[15]);
        col = atoi(&map_data[17]);
        coupled_switch.knob2_indices = {row, col};

        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[16]);
        col = atoi(&map_data[18]);
        coupled_switch.switch_indices = {row, col};

        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[18]);
        col = atoi(&map_data[20]);
        coupled_switch.affected_indices = {row, col};

        fscanf(fptr, "%s", map_data);
        bitfield = atoi(&map_data[18]);
        coupled_switch.obstacle_affected = bitfield;

        coupled_switch.knob1_on = false;
        coupled_switch.knob2_on = false;
        coupled_switch.enabled = true;
    }
    else
        coupled_switch.enabled = false;

    // read static portals if present
    // STATIC_PORTALS_ENABLED=%d
    //                        23
    int sp_enabled = 0;
    fscanf(fptr, "%s", map_data);
    sp_enabled = atoi(&map_data[23]);
    if(sp_enabled)
    {
        // sp1_position=%d,%d
        //              13, 15 
        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[13]);
        col = atoi(&map_data[15]);
        static_portal_pair.portal_1 = {row, col};

        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[13]);
        col = atoi(&map_data[15]);
        static_portal_pair.portal_2 = {row, col};

        static_portal_pair.enabled = true;
    }
    else
        static_portal_pair.enabled = false;

    // DYNAMIC_PORTALS_ENABLED=%d
    //                         24
    int dp_enabled = 0;
    fscanf(fptr, "%s", map_data);
    dp_enabled = atoi(&map_data[24]);
    if(dp_enabled)
        dynamic_portals_enabled = true;
    else
        dynamic_portals_enabled = false;
    
    Toggle(real_map, player);
}

void
Editor::HandleEvents(SDL_Event* e, Tilemap* real_map, Player* player)
{
    if(e->type == SDL_KEYDOWN)
    {
        uint32 obstacle_mask = 0xFFFFFF0F;

        // handle tile selection with wasd.
        if(e->key.keysym.sym == SDLK_w)
        {
            if(curr_indices.row > 0)
                curr_indices.row -= 1;
        }
        else if(e->key.keysym.sym == SDLK_a)
        {
            if(curr_indices.col > 0)
                curr_indices.col -= 1;
        }
        else if(e->key.keysym.sym == SDLK_s)
        {
            if(curr_indices.row < ROWS - 1)
                curr_indices.row += 1;
        }
        else if(e->key.keysym.sym == SDLK_d)
        {
            if(curr_indices.col < COLS - 1)
                curr_indices.col += 1;
        }

        // place directions with ijkl
        else if(e->key.keysym.sym == SDLK_i)
        {
            curr_tile->bitfield ^= Tilemap::UP;
        }
        else if(e->key.keysym.sym == SDLK_j)
        {
            curr_tile->bitfield ^= Tilemap::LEFT;
        }
        else if(e->key.keysym.sym == SDLK_k)
        {
            curr_tile->bitfield ^= Tilemap::DOWN;
        }
        else if(e->key.keysym.sym == SDLK_l)
        {
            curr_tile->bitfield ^= Tilemap::RIGHT;
        }

        // place obstacles with arrow keys
        /* NOTE:
           If left obstacle is set and left is pressed, toggle it off.
           If left obstacle is unset and left is pressed, mask all
           obstacle bits off and then set the left obstacle.
        */ 
        
        else if(e->key.keysym.sym == SDLK_UP)
        {
            if(IS_ON(curr_tile->bitfield, Tilemap::UP_OBSTACLE))
            {
                curr_tile->bitfield ^= Tilemap::UP_OBSTACLE;
            }
            else
            {
                curr_tile->bitfield &= obstacle_mask;
                curr_tile->bitfield ^= Tilemap::UP_OBSTACLE;
            }
        }
        else if(e->key.keysym.sym == SDLK_LEFT)
        {
            if(IS_ON(curr_tile->bitfield, Tilemap::LEFT_OBSTACLE))
            {
                curr_tile->bitfield ^= Tilemap::LEFT_OBSTACLE;
            }
            else
            {
                curr_tile->bitfield &= obstacle_mask;
                curr_tile->bitfield ^= Tilemap::LEFT_OBSTACLE;
            }
        }
        else if(e->key.keysym.sym == SDLK_DOWN)
        {
            if(IS_ON(curr_tile->bitfield, Tilemap::DOWN_OBSTACLE))
            {
                curr_tile->bitfield ^= Tilemap::DOWN_OBSTACLE;
            }
            else
            {
                curr_tile->bitfield &= obstacle_mask;
                curr_tile->bitfield ^= Tilemap::DOWN_OBSTACLE;
            }
        }
        else if(e->key.keysym.sym == SDLK_RIGHT)
        {
            if(IS_ON(curr_tile->bitfield, Tilemap::RIGHT_OBSTACLE))
            {
                curr_tile->bitfield ^= Tilemap::RIGHT_OBSTACLE;
            }
            else
            {
                curr_tile->bitfield &= obstacle_mask;
                curr_tile->bitfield ^= Tilemap::RIGHT_OBSTACLE;
            }
        }

        else if(e->key.keysym.sym == SDLK_e)
        {
            curr_tile->bitfield ^= Tilemap::END;
        }

        else if(e->key.keysym.sym == SDLK_b)
        {
            curr_tile->bitfield ^= Tilemap::BOMB1;
        }
        
        else if(e->key.keysym.sym == SDLK_BACKQUOTE)
        {
            Save();
        }

        else if(e->key.keysym.sym == SDLK_TAB)
        {
            Load(real_map, player);
        }

        else if(e->key.keysym.sym == SDLK_c)
        {
            if(EQUAL_INDEX(&coupled_switch.knob1_indices, &curr_indices))
                coupled_switch.knob1_indices = {255, 255};
            else
                coupled_switch.knob1_indices = curr_indices;
        }

        else if(e->key.keysym.sym == SDLK_v)
        {
            if(EQUAL_INDEX(&coupled_switch.knob2_indices, &curr_indices))
                coupled_switch.knob2_indices = {255, 255};
            else
                coupled_switch.knob2_indices = curr_indices;
        }

        else if(e->key.keysym.sym == SDLK_x)
        {
            if(EQUAL_INDEX(&coupled_switch.switch_indices, &curr_indices))
                coupled_switch.switch_indices = {255, 255};
            else
                coupled_switch.switch_indices = curr_indices;
        }

        else if(e->key.keysym.sym == SDLK_z)
        {
            if(EQUAL_INDEX(&coupled_switch.affected_indices, &curr_indices))
            {
                coupled_switch.affected_indices = {255, 255};
                coupled_switch.obstacle_affected = 0;
            }
            else
            {
                coupled_switch.affected_indices = curr_indices;
                
                uint32 obstacle_mask = Tilemap::UP_OBSTACLE | Tilemap::DOWN_OBSTACLE | Tilemap::RIGHT_OBSTACLE | Tilemap::LEFT_OBSTACLE;
                switch(curr_tile->bitfield & obstacle_mask)
                {
                case Tilemap::UP_OBSTACLE:
                {
                    coupled_switch.obstacle_affected = Tilemap::UP_OBSTACLE;
                }break;

                case Tilemap::DOWN_OBSTACLE:
                {
                    coupled_switch.obstacle_affected = Tilemap::DOWN_OBSTACLE;
                }break;

                case Tilemap::RIGHT_OBSTACLE:
                {
                    coupled_switch.obstacle_affected = Tilemap::RIGHT_OBSTACLE;    
                }break;

                case Tilemap::LEFT_OBSTACLE:
                {
                    coupled_switch.obstacle_affected = Tilemap::LEFT_OBSTACLE;
                }break;
                }
            }
                
        }
        
        else if(e->key.keysym.sym == SDLK_u)
        {
            if(EQUAL_INDEX(&static_portal_pair.portal_1, &curr_indices))
            {
                static_portal_pair.portal_1 = {255, 255};
            }
            else
                static_portal_pair.portal_1 = curr_indices;
        }

        else if(e->key.keysym.sym == SDLK_o)
        {
            if(EQUAL_INDEX(&static_portal_pair.portal_2, &curr_indices))
            {
                static_portal_pair.portal_2 = {255, 255};
            }
            else
                static_portal_pair.portal_2 = curr_indices;
        }

        else if(e->key.keysym.sym == SDLK_CAPSLOCK)
        {
            if(dynamic_portals_enabled)
            {
                Log("Dynamic portals disabled.");
                dynamic_portals_enabled = false;
            }
            else
            {
                Log("Dynamic portals enabled.");
                dynamic_portals_enabled = true;
            }
                
        }

        else if(e->key.keysym.sym == SDLK_m)
        {
            Log("Enter max moves for player.");
            scanf("%d", &move_limit);
        }
    }
}

void
Editor::UpdateAndRender(SDL_Renderer* Renderer)
{
    curr_tile = &map[curr_indices.row][curr_indices.col];
    
    // draw the grid.
    SDL_SetRenderDrawColor(Renderer, 0x00, 0xFF, 0x00, 0xFF);
    for(int i = 1; i < COLS + 1; i++)
    {
        SDL_RenderDrawLine(Renderer, TILE_WIDTH*i, 0,
                           TILE_WIDTH*i, TILE_WIDTH*ROWS);
    }
    for(int i = 1; i < ROWS; i++)
    {
        SDL_RenderDrawLine(Renderer, 0, TILE_WIDTH*i,
                           TILE_WIDTH*COLS, TILE_WIDTH*i);
    }
    
    // draw outline for currently selected tile.
    {
        SDL_SetRenderDrawColor(Renderer, 0xA7, 0x28, 0x53, 0xFF);
        SDL_Rect curr_outline = {curr_indices.col*TILE_WIDTH,
                                 curr_indices.row*TILE_WIDTH,
                                 TILE_WIDTH, TILE_WIDTH};
        
        SDL_RenderDrawRect(Renderer, &curr_outline);

        curr_outline.x -= 1;
        curr_outline.y -= 1;
        curr_outline.w += 2;
        curr_outline.h += 2;

        SDL_RenderDrawRect(Renderer, &curr_outline);
        
        curr_outline.x -= 1;
        curr_outline.y -= 1;
        curr_outline.w += 2;
        curr_outline.h += 2;
        
        SDL_RenderDrawRect(Renderer, &curr_outline);

        curr_outline.x += 3;
        curr_outline.y += 3;
        curr_outline.w -= 5;
        curr_outline.h -= 5;

        SDL_RenderDrawRect(Renderer, &curr_outline);
    }

    coupled_switch.UpdateAndRender(Renderer, true, true, true);
    static_portal_pair.UpdateAndRender(Renderer, true, true);
    
    // draw the tiles
    SDL_Rect temp = {0};
    for(int row = 0; row < ROWS; row++)
    {
        for(int col = 0; col < COLS; col++)
        {
            uint32 bf = map[row][col].bitfield;

            // render directions
            if(IS_ON(bf, Tilemap::RIGHT))
            {
                temp = {TILE_WIDTH*(col+1) - 48,
                        TILE_WIDTH*row + TILE_WIDTH/2 - 16,
                        32, 32};
                SDL_RenderCopy(Renderer, Game::right_sprite, NULL, &temp);
            }
            if(IS_ON(bf, Tilemap::LEFT))
            {
                temp = {TILE_WIDTH*col + 16,
                        TILE_WIDTH*row + TILE_WIDTH/2 - 16,
                        32, 32};
                SDL_RenderCopy(Renderer, Game::left_sprite, NULL, &temp);
            }
            if(IS_ON(bf, Tilemap::DOWN))
            {
                temp = { TILE_WIDTH*col + TILE_WIDTH/2 - 16,
                         TILE_WIDTH*(row+1) - 48,
                         32, 32 };
                SDL_RenderCopy(Renderer, Game::down_sprite, NULL, &temp);
            }
            if(IS_ON(bf, Tilemap::UP))
            {
                temp = { TILE_WIDTH*col + TILE_WIDTH/2 - 16,
                         TILE_WIDTH*row + 16,
                         32, 32 };
                SDL_RenderCopy(Renderer, Game::up_sprite, NULL, &temp);
            }

            // render obstacles
            {
                uint32 mask = Tilemap::UP_OBSTACLE | Tilemap::RIGHT_OBSTACLE |
                    Tilemap::LEFT_OBSTACLE | Tilemap::DOWN_OBSTACLE;

                MatrixIndex obs_tile_index = {row, col};
                SDL_Rect crop_rect = {10, 14, 38, 60};
                switch((bf & mask))
                {
                case Tilemap::UP_OBSTACLE:
                {
                    temp = {TILE_WIDTH*col + TILE_WIDTH/2 - 16,
                            TILE_WIDTH*row,
                            32, 32};
                    if(VALID_INDEX(&coupled_switch.affected_indices) &&
                       EQUAL_INDEX(&coupled_switch.affected_indices, &obs_tile_index))
                    {
                        temp.y += 20;
                        temp.w = 38;
                        temp.h = 60;
                        SDL_RenderCopy(Renderer, Game::dynamite_sprite, &crop_rect, &temp);
                    }
                    else
                    {
                        SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);

                        if(IS_ON(bf, Tilemap::TWO_OBSTACLES))
                        {
                            temp.y += 32;
                            bf ^= Tilemap::TWO_OBSTACLES;
                            SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                        }
                    }
                
                }
                break;

                case Tilemap::DOWN_OBSTACLE:
                {
                    temp = {TILE_WIDTH*col + TILE_WIDTH/2 - 16,
                            TILE_WIDTH*(row+1) - 32,
                            32, 32};
                    if(VALID_INDEX(&coupled_switch.affected_indices) &&
                       EQUAL_INDEX(&coupled_switch.affected_indices, &obs_tile_index))
                    {
                        temp.y -= 20;
                        temp.w = 38;
                        temp.h = 60;
                        SDL_RenderCopy(Renderer, Game::dynamite_sprite, &crop_rect, &temp);
                    }
                    else
                    {
                        SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                        if(IS_ON(bf, Tilemap::TWO_OBSTACLES))
                        {
                            temp.y -= 32;
                            bf ^= Tilemap::TWO_OBSTACLES;
                            SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                        }
                    }
                }
                break;

                case Tilemap::RIGHT_OBSTACLE:
                {
                    temp = {TILE_WIDTH*(col+1) - 32,
                            TILE_WIDTH*row + TILE_WIDTH/2 - 16,
                            32, 32};
                    if(VALID_INDEX(&coupled_switch.affected_indices) &&
                       EQUAL_INDEX(&coupled_switch.affected_indices, &obs_tile_index))
                    {
                        temp.x -= 20;
                        temp.w = 38;
                        temp.h = 60;
                        SDL_RenderCopy(Renderer, Game::dynamite_sprite, &crop_rect, &temp);
                    }
                    else
                    {
                        SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                        if(IS_ON(bf, Tilemap::TWO_OBSTACLES))
                        {
                            temp.x -= 32;
                            bf ^= Tilemap::TWO_OBSTACLES;
                            SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                        }
                    }
                }
                break;

                case Tilemap::LEFT_OBSTACLE:
                {
                    temp = {TILE_WIDTH*col,
                            TILE_WIDTH*row + TILE_WIDTH/2 - 16,
                            32, 32};
                    if(VALID_INDEX(&coupled_switch.affected_indices) &&
                       EQUAL_INDEX(&coupled_switch.affected_indices, &obs_tile_index))
                    {
                        temp.x += 20;
                        temp.w = 38;
                        temp.h = 60;
                        SDL_RenderCopy(Renderer, Game::dynamite_sprite, &crop_rect, &temp);
                    }
                    else
                    {
                        SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                        if(IS_ON(bf, Tilemap::TWO_OBSTACLES))
                        {
                            temp.x += 32;
                            bf ^= Tilemap::TWO_OBSTACLES;
                            SDL_RenderCopy(Renderer, Game::rock_sprite, NULL, &temp);
                        }
                    }
                }
                break;
                } // switch for obstacle display
            }
            
            if(IS_ON(bf, Tilemap::BOMB1))
            {
                temp = {TILE_WIDTH*col + 16,
                        TILE_WIDTH*row + 16,
                        32, 32};
                SDL_RenderCopy(Renderer, Game::bomb_sprite, NULL, &temp);
            }
            
            if(IS_ON(bf, Tilemap::END))
            {
                temp = {TILE_WIDTH*col + TILE_WIDTH/2 + 16,
                        TILE_WIDTH*row + TILE_WIDTH/2 - 16,
                        32, 32};
                SDL_RenderCopy(Renderer, Game::end_sprite, NULL, &temp);
            }
        }
    }
}
