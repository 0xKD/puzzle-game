#pragma once
#include "Prototype.hpp"
#include "Utils.hpp"
#include "Tilemap.hpp"

class Tilemap;

class PortalPair
{
public:
    MatrixIndex portal_1;
    MatrixIndex portal_2;
    bool enabled;
    
    PortalPair();
    void Setup(Tilemap* tilemap); // needs to be called every reset after initializing tilemap
    void UpdateAndRender(SDL_Renderer* Renderer, bool p1_visible, bool p2_visible); // should be called before tilemap render
};

class CoupledSwitch
{
public:
    /*NOTE:
      This assumes that the affector tile will have no
      other obstacles apart from the one affected by the
      switch.
    */
    MatrixIndex knob1_indices; // blue switch
    MatrixIndex knob2_indices; // orange switch
    MatrixIndex switch_indices;
    MatrixIndex affected_indices;
    uint8 obstacle_affected;
    bool knob1_on, knob2_on;
    bool enabled;

    CoupledSwitch();
    void Setup();
    void UpdateAndRender(SDL_Renderer* Renderer, bool knob1_visible, bool knob2_visible, bool switch_visible);
};
