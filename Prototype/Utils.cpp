#include "Prototype.hpp"
#include "Utils.hpp"
#include <stdlib.h>

void strreverse(char* begin, char* end) {
	
	char aux;
	
	while(end>begin)
	
		aux=*end, *end--=*begin, *begin++=aux;
	
}
	
void myitoa(int value, char* str, int base) {
	
	static char num[] = "0123456789abcdefghijklmnopqrstuvwxyz";
	
	char* wstr=str;
	
	int sign;
	

	
	// Validate base
	
	if (base<2 || base>35){ *wstr='\0'; return; }
	

	
	// Take care of sign
	
	if ((sign=value) < 0) value = -value;
	

	
	// Conversion. Number is reversed.
	
	do *wstr++ = num[value%base]; while(value/=base);
	
	if(sign<0) *wstr++='-';
	
	*wstr='\0';
	

	
	// Reverse string
	
	strreverse(str,wstr-1);
	
}

bool IS_ON(uint32 bitfield, uint32 mask)
{
    if((bitfield & mask) == mask)
        return true;
    else
        return false;
}

bool IS_OFF(uint32 bitfield, uint32 mask)
{
    if((bitfield & mask) == 0)
        return true;
    else
        return false;
}

bool VALID_INDEX(MatrixIndex* indices)
{
    if(indices->row == 255 && indices->col == 255)
    {
        return false;
    }
    // NOTE: dependency, switch ROWS, COLS to prototype.hpp
    else if(indices->row < 5 && indices->col < 8)
    {
        return true;
    }
    else
        return false;
}

bool EQUAL_INDEX(MatrixIndex* first, MatrixIndex* second)
{
    if(first->row == second->row && first->col == second->col)
        return true;
    else
        return false;
}

TextSprite::TextSprite()
{
    cached_text[0] = '0';
    text_color = {0xFF, 0x00, 0x00, 0xFF};
}

void
TextSprite::Init(TTF_Font* numeric_font, SDL_Renderer* Renderer)
{
    LoadText(numeric_font, Renderer);
}

void
TextSprite::UpdateAndRender(int x, int y, int num, TTF_Font* numeric_font, SDL_Renderer* Renderer)
{
    SDL_Rect temp = {x, y, width, height};
    int cached_num = atoi(&cached_text[0]);
    if(num != cached_num)
    {
        myitoa(num, cached_text, 10);
        LoadText(numeric_font, Renderer);
        temp.w = width;
        temp.h = height;
        SDL_RenderCopy(Renderer, tex, NULL, &temp);
    }
    else
        SDL_RenderCopy(Renderer, tex, NULL, &temp);
}

void
TextSprite::LoadText(TTF_Font* numeric_font, SDL_Renderer* Renderer)
{
    SDL_Surface* temp_surf = TTF_RenderText_Solid(numeric_font,
                                                  cached_text,
                                                  text_color);
    if(temp_surf == NULL)
    {
        Log("Text surface creation failed.\nSystem message - %s\n", GetError());
    }
    else
    {
        tex = SDL_CreateTextureFromSurface(Renderer, temp_surf);
        width = temp_surf->w;
        height = temp_surf->h;
        SDL_FreeSurface(temp_surf);
    }
}

/*
Initialize all SDL systems.
Preps the window and renderer.
Returns 1 on success, 0 on failure.
 */
int
InitSDL(SDL_Window** Window, SDL_Renderer** Renderer, char* title, int width, int height)
{
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0){
        Log("SDL didnt initialize. System message -- %s\n", GetError());
        return 0;
    }
    else{
        *Window = SDL_CreateWindow(title,
                                   SDL_WINDOWPOS_UNDEFINED,
                                   SDL_WINDOWPOS_UNDEFINED,
                                   width, height,
                                   SDL_WINDOW_SHOWN);
        if (*Window == NULL){
            Log("Window creation error.\nSystem message -- %s\n", GetError());
            return 0;
        }
        else{
            *Renderer = SDL_CreateRenderer(*Window, -1, SDL_RENDERER_ACCELERATED);
            if (*Renderer == NULL)
            {
                Log("Renderer initialization failed.\nSystem message - %s\n", GetError());
                return 0;
            }
            else
            {
                if(IMG_Init(IMG_INIT_PNG) == IMG_INIT_PNG)
                {
                    if(TTF_Init() == -1)
                    {
                        Log("SDL_ttf could not be initialized!\nSystem message - %s\n", GetError());
                        return 0;
                    }
                    else
                    {
                        Log("All Systems Go.");
                        return 1;
                    }
                    
                    // if(IS_ON(Mix_Init(MIX_INIT_FLAC), MIX_INIT_FLAC))
                    // {
                    //     if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) == -1)
                    //     {
                    //         Log("Audio opening failed.\nSystem message - %s\n", GetError());
                    //         return 0;
                    //     }
                    //     Log("All Systems Go.");
                    //     return 1;
                    // }
                    // else
                    // {
                    //     Log("Audio module initialization failed.\nSystem message - %s\n", GetError());
                    //     return 0;
                    // }
                }
                else
                {
                    Log("Image module initialization failed.\nSystem message - %s\n", GetError());
                    return 0;
                }
            }
        }
    }
}

SDL_Texture* LoadImage(char* filename, SDL_Renderer* Renderer)
{
    SDL_Texture* temp = NULL;
    
    SDL_Surface* surf = IMG_Load(filename);
    if(surf == NULL)
    {
        Log("Image loader failed.\n");
    }
    else
    {
        temp = SDL_CreateTextureFromSurface(Renderer, surf);
        if(temp == NULL)
        {
            Log("Texture creation from surface failed.\n");
        }
        else
        {
            SDL_FreeSurface(surf);
        }
    }
    return temp;
}

SDL_Texture* LoadImageWithColorKey(char* filename, SDL_Renderer* Renderer, uint8 r, uint8 g, uint8 b)
{
    SDL_Texture* temp = NULL;
    
    SDL_Surface* surf = IMG_Load(filename);
    if(surf == NULL)
    {
        Log("Image loader failed.\n");
    }
    else
    {
        SDL_SetColorKey(surf, SDL_TRUE, SDL_MapRGB(surf->format, r, g, b));
        temp = SDL_CreateTextureFromSurface(Renderer, surf);
        if(temp == NULL)
        {
            Log("Texture creation from surface failed.\n");
        }
        else
        {
            SDL_FreeSurface(surf);
        }
    }
    return temp;
}
